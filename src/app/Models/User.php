<?php

namespace Neider\MiniFrameworkPhp\Models;

use Illuminate\Database\Eloquent\Model;

class User Extends Model {
    protected $table = 'users';

    protected $fillable = ['name','email'];
}
