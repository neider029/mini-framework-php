<?php

namespace Neider\MiniFrameworkPhp\Controllers;

use Illuminate\View\Factory;
use Neider\MiniFrameworkPhp\Models\User;

class HomeController
{

    public function index(Factory $factory): void
    {
        $users = User::all();
        echo $factory->make(view: 'home',data:compact(var_name:'users'));
    }
}