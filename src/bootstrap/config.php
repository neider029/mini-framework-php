<?php

use Illuminate\Events\Dispatcher;
use Neider\MiniFrameworkPhp\Controllers\HomeController;
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\Engines\CompilerEngine;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;



return [
    'database' => [
        'driver'    => 'mysql',
        'host'      => env('DB_HOST'),
        'database'  => env('DB_DATABASE'),
        'username'  => env('DB_USERNAME'),
        'password'  => env('DB_PASSWORD'),
        'port'      => env('DB_PORT'),
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ],

    \Illuminate\Database\Capsule\Manager::class => function ($container) {
        $manager = new \Illuminate\Database\Capsule\Manager;
        $manager->addConnection($container->get('database'));
        $manager->setAsGlobal();
        $manager->bootEloquent();

        return $manager;
    },


    HomeController::class => DI\create(),


    FileViewFinder::class => function ($container) {
        $filesystem = new Filesystem;

        return new FileViewFinder($filesystem, [__DIR__ . '/../resources/views']);
    },
    CompilerEngine::class => function ($container) {
        $filesystem = new Filesystem;
        $compiler = new BladeCompiler($filesystem, __DIR__ . '/../storage/views', shouldCache: false);

        return new CompilerEngine($compiler);
    },
    Factory::class => function ($container) {
        $resolver = new EngineResolver;
        $resolver->register('blade', function () use ($container) {
            return $container->get(CompilerEngine::class);
        });
        $finder = $container->get(FileViewFinder::class);

        return new Factory($resolver, $finder, new Dispatcher);
    },
];