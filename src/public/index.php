<?php

use FastRoute\Dispatcher;

error_reporting(error_level:E_ALL);
ini_set(option:'display_errors',value: 1);

$container = require __DIR__ . '/../bootstrap/container.php';

$dotenv = Dotenv\Dotenv::createImmutable(paths:'../');
$dotenv->load();

$container->get(\Illuminate\Database\Capsule\Manager::class);

$dispatcher = require base_path(path: 'routes/web.php');


$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = parse_url($_SERVER['REQUEST_URI'], component: PHP_URL_PATH);
$route = $dispatcher->dispatch($httpMethod, $uri);


switch ($route[0]) {
    ## route not found
    case Dispatcher::NOT_FOUND: {
        echo "Ruta no encontrada";
        break;
    }
    ## method not allowed
    case Dispatcher::METHOD_NOT_ALLOWED: {
        echo "Método HTTP no permitido";
        break;
    }
    ## route found
    case Dispatcher::FOUND: {
        $controller = $route[1];
        $params = $route[2];
        $container->call($controller, $params);
        break;
    }
}