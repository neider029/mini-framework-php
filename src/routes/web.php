<?php

use function FastRoute\simpleDispatcher;
use FastRoute\RouteCollector;
use Neider\MiniFrameworkPhp\Controllers\HomeController;


return simpleDispatcher(function (RouteCollector $route) {

    $route->addRoute(httpMethod: 'GET', route: '/', handler: [HomeController::class, 'index']);
});